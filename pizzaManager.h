// Tariq Khandwalla
// Date: Tuesday April 23, 2023
// A02 CS163
// Purpose: This file contains the class definition for the PizzaManager class.

#ifndef PIZZAMANAGER_H
#define PIZZAMANAGER_H

#include <iostream>

using namespace std;

struct Topping {
    char name[50];
    int amount; // number of ounces or slices
    int calories;
    char additionalInfo[50];
};

class Stack {
private:
    static const int MAX_SIZE = 5; // Maximum size of each array
    struct Node {
        Topping toppings[MAX_SIZE];
        int top; // Index of the top element in the array
        Node* next;
    };
    Node* topNode;

public:
    Stack();
    ~Stack();
    void push(const Topping& topping);
    void pop();
    Topping peek();
    void display();
};

struct Pizza {
    char name[50];
    char sauceType[50];
    int numToppings;
    char additionalInfo[50];
};

class Queue {
private:
    struct Node {
        Pizza pizza;
        Node* next;
    };
    Node* rear; // Pointer to the last pizza in the queue

public:
    Queue();
    ~Queue();
    void enqueue(const Pizza& pizza);
    void dequeue();
    Pizza peek();
    void display();
};

#endif // PIZZAMANAGER_H
